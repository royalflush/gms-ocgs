///scr_find_droppable(x, y)
/*
**  Description
**      Finds the highest droppable at the specified position.
**      Droppables are obj_hand and obj_deck
**    
**  Arguments
**      x   real    The x coordinate to check
**      y   real    The y coordinate to check
**
**  Returns
**      The ID of the found droppable, or noone if none was found.
**
*/

var xCoord = argument0;
var yCoord = argument1;

var closest = noone;

with(obj_deck)
    if ( position_meeting_rect(xCoord, yCoord, id) && ( closest == noone || closest.depth > id.depth) ) {
        closest = id;
    }

//Code for the hand.
if( yCoord > view_yview[1])
{
    closest = global.hand_object;
}
       
    /*
with(global.hand_object) {
    //Check points even outside the view (left, right, and below).
    if( yCoord > (view_yview[0] + view_hview[0] - handHeight) ) {
        closest = id;   
    }
}
 */   
return closest;






