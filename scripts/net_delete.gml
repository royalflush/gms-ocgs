///net_delete(data)

var data = argument0;

var net_id = buffer_read(data, buffer_string);

var local_inst = syncMap[? net_id];

ds_map_delete(syncMap, net_id);

with( local_inst )
{
    instance_destroy();
}
