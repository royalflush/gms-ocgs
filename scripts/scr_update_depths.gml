///scr_update_depths()

// Update every free card's depth to the inverse of its position.
// and normalize so the closest card is at depth == 1
for(i = 0; i < ds_list_size(global.freeCards); i++) {
    global.freeCards[| i].depth = (-1*i) + ds_list_size(global.freeCards);
}

// Update every hand card's depth based on its position
// normalise so every card is between -1.5 and -2.5, with the farthest right
// cards being higher.
for(i = 0; i < ds_list_size(global.handCards); i++) {
    global.handCards[| i].depth = ((i/ds_list_size(global.handCards)*-1) - 1.5);// change this 1.5
}

// Update every search/view card's depth based on its position
// normalise so every card is between -3.5 and -4.5, with the farthest right
// cards being higher.

for(i = 0; i < ds_list_size(global.searchCards); i++) {
    global.searchCards[| i].depth = ((i/ds_list_size(global.searchCards)*-1) - 3.5);// change this 1.5
}
