///scr_cleanup_tmp()
/*
**  Description
**      Deletes the files in the \tmp\ subdirectory.
**
**  Arguments
**      <none>
**
**  Returns
**      <none>
**
*/

var delFileName = file_find_first(working_directory + "tmp\*.*", 0);
var delPath = working_directory + "tmp\" + delFileName;
while(delFileName != "") {
    file_delete(delPath);
    delFileName = file_find_next();
    delPath = working_directory + "tmp\" + delFileName;
}
