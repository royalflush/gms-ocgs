///net_hand_remove(data)

var data = argument0;

var net_id = buffer_read(data, buffer_string);

var local_inst = syncMap[? net_id];

hand_size--;

/*
with( local_inst )
{
    x = 50;
    y = 50;
}
