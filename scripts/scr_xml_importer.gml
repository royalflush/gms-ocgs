///scr_xml_importer(path)
/*
**  Description
**      Load cards from a .deck's XML file
**
**  Arguments
**      path    string      Path to the .deck file to import
**
**  Returns
**      obj_deck            The resulting deck
*/

// Get the player's .deck file.
//If they don't select anything, destroy this instance and don't do the rest of this event.
var filepath = argument0;
if(filepath == "") exit;

// Unzip/"move" the data to the working dir. If there's an error, don't do the rest of this event.
var num = zip_unzip(filepath, "tmp\" );
if( num <= 0) {
    show_message_async("Couldn't extract the deck!");
    scr_cleanup_tmp(); // In case SOMETHING was extracted
    exit;
}

// Check to make sure info.xml exists.
if( !file_exists(working_directory + "tmp\info.xml")) {
    show_message_async("Couldn't find info.xml!");
    scr_cleanup_tmp(); // clean out directory
    exit;
}

// Open info.xml as a file, and convert the file's contents to a string.
var file = file_text_open_read(working_directory + "tmp\info.xml");
var xml  = "";
while (!file_text_eof(file))
{
    xml += file_text_readln(file);
}
file_text_close(file);

//type: xf_node
var root = xf_read_xml(xml);    // Parse the xml data.
var type;                       // e.g. "<global>, <card>"
var attrib;                     // e.g. "<img>, <height>, <back>"
var value;                      // e.g. "88, image.jpg" but it's still a node (not string) until xf_write_xml is used.

// type: sprite
var sharedFront = noone;
var sharedBack = noone;

// type: real
var sharedWidth = 0;
var sharedHeight = 88;

var path = "";

// used for error checking
var fileError = false;
var dim = 0;

// Find all the GLOBALs to set up shared values.
// GLOBALS are considered "more important" and so if there's an error in one, abandon the entire import.
// Otherwise the user might be spammed with "missing back" errors for EVERY card that depends on a global.
// or something similar
for(i = 0; i < xf_size(root); i++ ) {
    type = xf_get_child(root, i);
    if( xf_get_value(type) == "GLOBAL") {        
        for(j = 0; j < xf_size(type); j++ ) {
            attrib = xf_get_child(type, j);
            value = xf_get_child(attrib, 0);
            
            // Found img attribute
            if( ( xf_get_value(attrib) == "img") && (xf_get_type(value) == xf_type_text) ) {
                path = working_directory + "tmp\" + xf_write_xml(value);
                
                // check to make sure the file exists.
                if( !file_exists( path ) ) {
                    show_message_async('ERROR#File "' + path + '" not found.');
                    scr_cleanup_tmp();
                    exit;
                }
                else {
                    sharedFront = path;
                /*
                    sprite = sprite_add(path, 0, false, false, 0, 0);
                    
                    // Check to make sure the file is a real image
                    if( !sprite_exists(sprite) ) {
                        show_message_async("Error for file " + path + ": Error importing front sprite!");
                    scr_cleanup_tmp();
                    exit;
                    }
                    else // No errors
                        sharedFront = sprite;
                        */
                }
            }
            
            // Found back attribute
            if( ( xf_get_value(attrib) == "back") && (xf_get_type(value) == xf_type_text) ) {
                path = working_directory + "tmp\" + xf_write_xml(value);
                
                // check to make sure the file exists.
                if( !file_exists( path ) ) {
                    show_message_async('ERROR#File "' + path + '" not found.');
                    scr_cleanup_tmp();
                    exit;
                }
                else {
                    sharedBack = path;
                /*
                    sprite = sprite_add(path, 0, false, false, 0, 0);
                    
                    // Check to make sure the file is a real image
                    if( !sprite_exists(sprite) ) {
                        show_message_async("Error for file " + path + ": Error importing back sprite!");
                        scr_cleanup_tmp();
                        exit;
                    }
                    else // No errors
                        sharedBack = sprite;
                        */
                }
            }
            
            // Found height attribute
            if( (xf_get_value(attrib) == "height") && (xf_get_type(value) == xf_type_text) ) {
                dim = real(xf_write_xml(value));
                if(dim == 0) { // Something is wrong the the dim is recognized as 0.
                    show_message_async("ERROR: GLOBAL height receognized as 0! Are you sure it's a number?");
                    scr_cleanup_tmp();
                    exit;
                }
                else { // No errors.
                    sharedHeight = dim;
                }
            }
            
            // Found width attribute  
            if( ( xf_get_value(attrib) == "width") && (xf_get_type(value) == xf_type_text) ) {
                dim = real(xf_write_xml(value));
                if(dim == 0) { // Something is wrong the the dim is recognized as 0.
                    show_message_async("ERROR: GLOBAL width receognized as 0! Are you sure it's a number?");
                    scr_cleanup_tmp();
                    exit;
                }
                else { // No errors.
                    sharedWidth = dim;
                }
            }
        }        
    }
}

// Create a deck, create a ds_map to represent each card, and add the map to the deck.
var deck = instance_create(mouse_x, mouse_y, obj_deck);
var copies = 0;

var front = noone;
var back = noone;
var height = noone;
var width = noone;

var sprite = noone;
var back = noone;

for(i = 0; i < xf_size(root); i++) {
    path = "";
    fileError = false;
    type = xf_get_child(root, i);
    if( xf_get_value(type) == "card")
    {
        copies = xf_get_attr(type, "copies");       // Get the number of copies of the card
        if(copies == noone )                           // If copies isn't listed, it only wants one.
            copies = 1;  
        
        // Set to the shared values first, so if they are never overwriten the shared value will be used.                
        front = sharedFront;
        back = sharedBack;
        width = sharedWidth;
        height = sharedHeight;
        
        for(k = 0; k < xf_size(type); k++ ) {
            attrib = xf_get_child(type, k);
            value = xf_get_child(attrib, 0);
            
            // Check if current node is front image. If it is, error check it and import it.
            if( ( xf_get_value(attrib) == "img") && (xf_get_type(value) == xf_type_text) ) {
                path = working_directory + "tmp\" + xf_write_xml(value);
                
                // check to make sure the file exists.
                if( !file_exists( path ) ) {
                    show_message_async('ERROR#File "' + path + '" not found.');
                    fileError = true;
                    break;
                }
                else {
                
                    front = path;
                
                /*
                    sprite = sprite_add(path, 0, false, false, 0, 0);
                    
                    // Check to make sure the file is a real image
                    if( !sprite_exists(sprite) ) {
                        show_message_async("Error for file " + path + ": Error importing front sprite!");
                        fileError = true;
                        break;
                    }
                    else // No errors
                        front = sprite;
                        */
                }
            }

            // Check if current node is back image. If it is, error check it and import it.
            if( ( xf_get_value(attrib) == "back") && (xf_get_type(value) == xf_type_text) ) {
                path = working_directory + "tmp\" + xf_write_xml(value);
                
                // check to make sure the file exists.
                if( !file_exists( path ) ) {
                    show_message_async('ERROR#File "' + path + '" not found.');
                    fileError = true;
                    break;
                }
                else {
                    back = path;
                /*
                    sprite = sprite_add(path, 0, false, false, 0, 0);
                    
                    // Check to make sure the file is a real image
                    if( !sprite_exists(sprite) ) {
                        show_message_async("Error for file " + path + ": Error importing back sprite!");
                        fileError = true;
                        break;
                    }
                    else // No errors
                        back = sprite;
                        */
                }
            }
            
            // Check if current node is height. If it is, error check it an prepare it.
            if( (xf_get_value(attrib) == "height") && (xf_get_type(value) == xf_type_text) ) {
                dim = real(xf_write_xml(value));
                if(dim == 0) {
                    show_message_async("ERROR: height receognized as 0! Are you sure it's a number?");
                }
                else {
                    height = dim;
                    fileError = true;
                    break;
                }
            }
            
            // Check if current node is width. If it is, error check it an prepare it.
            if( ( xf_get_value(attrib) == "width") && (xf_get_type(value) == xf_type_text) ) {
                dim = real(xf_write_xml(value));
                if(dim == 0) {
                    show_message_async("ERROR: width receognized as 0! Are you sure it's a number?");
                }
                else {
                    width = dim;
                    fileError = true;
                    break;            
                }
            }
        }
        
        // Was there an error finding a file? If so, don't add any cards based on it.
        if( !fileError ) {
        
            sprite = sprite_add(front, 0,0,0,0,0);
            tmp = sprite_add(back, 0,0,0,0,0);
            sprite_merge(sprite, tmp);
            sprite_delete(tmp);
            sprite_net_id = sync_sprite(sprite, filename_name(front), filename_name(back) );
        
            for(j = 0; j < copies; j++) {
                
                // Error checking. Checking if a card managed to get through without having a side set.
                if( front == noone ) {
                    show_message_async("ERROR: A card did not have a front specified!#It will not be added to the deck.");
                }
                else if( back == noone) {
                    show_message_async("ERROR: A card did not have a back specified!#It will not be added to the deck.");
                }
                else {   // No errors        
                    var map = ds_map_create();
                    
                    map[? "net_id"] = net_hash();
                    //map[? "front"] = front;
                    //map[? "back"] = back;
                    map[? "sprite"] = sprite;
                    map[? "sprite_net_id"] = sprite_net_id;
                    map[? "height"] = height;
                    map[? "width"] = width;
                    
                    /*
                    map[? "front_offset_x"] = sprite_get_width(map[? "front"]) / 2;
                    map[? "front_offset_y"] = sprite_get_height(map[? "front"]) / 2;
                    
                    map[? "back_offset_x"] = sprite_get_width(map[? "back"]) / 2;
                    map[? "back_offset_y"] = sprite_get_height(map[? "back"]) / 2;
                    */
                    
                    map[? "offset_x"] = sprite_get_width(sprite) / 2;
                    map[? "offset_y"] = sprite_get_height(sprite) / 2;
                    
                    /*
                    map[? "front_scale_x"] = map[? "width"] / sprite_get_width(map[? "front"]);
                    map[? "front_scale_y"] = map[? "height"] / sprite_get_height(map[? "front"]);
                    
                    map[? "back_scale_x"] = map[? "width"] / sprite_get_width(map[? "back"]);
                    map[? "back_scale_y"] = map[? "height"] / sprite_get_height(map[? "back"]);
                    */
                    
                    map[? "scale_x"] = width / sprite_get_width(sprite);
                    map[? "scale_y"] = height / sprite_get_height(sprite);
                    
                    map[? "flipped"] = false;
                    map[? "scale"] = 2; // default value. Can be changed.
                                        
                    ds_list_add(deck.list, map);
                }
            }
        }
    }
}

deck.depth = 0;


// Once everything has been loaded, get rid of the files from the temporary directory.
//scr_cleanup_tmp();
return deck;

// Again, since //instance_destroy() was called at the beginning, this should be destroyed now.
// Even if it was exited early due to an error.
