/// scr_hand_insert(card)
/*
**  Description
**      Inserts the specified card into the correct position in the hand.
**
**  Arguments
**      card    obj_card    The card to insert.
**
**  Returns
**      <nothing>
**
*/

var card = argument0;

// search from left to right until we find a card who's position is less than ours.
// and put our card after it.
var tracker = 0;

while( tracker < ds_list_size(global.handCards) && card.x > global.handCards[| tracker].x )
    tracker++;
    
ds_list_insert(global.handCards, tracker, card);
ds_list_delete(global.freeCards, ds_list_find_index(global.freeCards, selected));  
