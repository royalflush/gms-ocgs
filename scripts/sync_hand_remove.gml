///sync_hand_remove(card)

if(global.multiplayer)
{
    var card = argument0;
    
    with(global.net_object)
    {
        buffer_seek(buffer, buffer_seek_start, 0);
        
        buffer_write(buffer, buffer_u8, sig.handRemove);
        
        buffer_write(buffer, buffer_string, card.net_id);
        
        if( !network_send_packet(socket, buffer, buffer_tell(buffer) ) )
        {
            debug("ERROR sending hand removal command.");
        }
    }
}


